﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace TikTak
{
	[Activity (Label = "TikTak", MainLauncher = true, Icon = "@drawable/icon",Theme = "@android:style/Theme.NoTitleBar")]
	public class MainActivity : Activity
	{


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
		
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			Button startButton = FindViewById<Button> (Resource.Id.startButton);
			Button instructionsButton = FindViewById<Button> (Resource.Id.instructionsButton);

			startButton.Click += (sender, ventArgs) => goToNiveaus ();
			instructionsButton.Click += (sender, eventArgs) => goToInstructionPage();
		}

		//All Events
		private void goToNiveaus(){
			var intent = new Intent(this, typeof(NiveauActivity));
			StartActivity(intent);
		}

		private void goToInstructionPage(){
			var intent = new Intent(this, typeof(InstructionsActivity));
			StartActivity(intent);
		}

	}

}


