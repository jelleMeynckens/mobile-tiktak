﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;

namespace TikTak
{

	public class ImageViewAdapter : BaseAdapter<ImageAudio> {
		IList<ImageAudio>items;
		GameActivity context;
		GridView gv;
		int niveau;

		public ImageViewAdapter(GameActivity context,GridView gv, IList<ImageAudio> items, int niveau) : base() {
			this.context = context;
			this.items = items;
			this.gv = gv;
			this.niveau = niveau;
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public override ImageAudio this[int position] {
			get { return items[position]; }
		}
		public override int Count {
			get { return items.Count; }
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView; // re-use an existing view, if one is available
			if (view == null) // otherwise create a new one
				view = context.LayoutInflater.Inflate(Resource.Layout.item, null);
			view.FindViewById<ImageView> (Resource.Id.grid_item_image).SetImageResource (items[position].imgid);
			view.FindViewById<ImageView> (Resource.Id.grid_item_image).SetFitsSystemWindows (true);
			items [position].imgid.ToString ();

			int param = gv.Width / niveau;
			int ncolumns = 2;
			if (gv.Height >= gv.Width && niveau == 1) {
				ncolumns = 1;
				param = System.Convert.ToInt16(gv.Width / 1.8);
			}
			else if (gv.Height < gv.Width) {
				if(niveau > 2) ncolumns = niveau;
				param = System.Convert.ToInt16 ((gv.Height / ((niveau * 2) / ncolumns)) / 1.2);
			}

			view.FindViewById<ImageView> (Resource.Id.grid_item_image).SetScaleType (ImageView.ScaleType.FitXy);
			gv.SetNumColumns (ncolumns);
			view.FindViewById<ImageView> (Resource.Id.grid_item_image).LayoutParameters = new RelativeLayout.LayoutParams (param, param);
			return view;
		}

		public void RefreshNewList(List<ImageAudio> newList){
			items = newList;
			NotifyDataSetChanged ();
		}
	}


}

