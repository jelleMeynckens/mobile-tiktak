﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Graphics;

namespace TikTak
{
	[Activity (Label = "TikTak",Theme = "@android:style/Theme.NoTitleBar")]			
	public class NiveauActivity : Activity
	{
		Button backButton;
		LinearLayout niveau1Button, niveau2Button, niveau3Button;

		ISharedPreferences data;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Niveaus); 

			data = GetSharedPreferences ("niveauscores", FileCreationMode.Private);

			backButton = FindViewById<Button> (Resource.Id.backToMainButton);
			niveau1Button = FindViewById<LinearLayout> (Resource.Id.niveau1Button);
			niveau2Button = FindViewById<LinearLayout> (Resource.Id.niveau2Button);
			niveau3Button = FindViewById<LinearLayout> (Resource.Id.niveau3Button);

			backButton.Click += (sender, eventArgs) => goBackToMain();
			niveau1Button.Click += (sender, eventargs) => startGame(1);
			niveau2Button.Click += (sender, eventargs) => startGame(2);
			niveau3Button.Click += (sender, eventargs) => startGame(3);

			updateButtonScores ();
		}

		protected override void OnResume() {
			base.OnResume ();
			updateButtonScores ();
		}

		private void goBackToMain(){
			base.OnBackPressed ();
		}

		private void startGame(int i){
			var intent = new Intent(this, typeof(GameActivity));
			intent.PutExtra ("niveau", i);
			StartActivityForResult(intent,1);
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data){
			base.OnActivityResult (requestCode, resultCode, data);
			if (requestCode == 1) {
				if (resultCode == Result.Ok) {
					int niveau = data.GetIntExtra ("niveau", 0);
					int score = data.GetIntExtra ("score",0);
					double i = (double)score / (niveau*5);
					int points = (int)(i*100);

					if (points > getNiveauScore (niveau)) {
						storeNiveauScore (niveau, points);
					}
					updateButtonScores ();
				}
			}
		}

		private void storeNiveauScore(int niv, int score) {
			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutInt ("scoreNiv"+niv, score);
			editor.Commit();
		}

		private int getNiveauScore(int niv) {
			return data.GetInt ("scoreNiv"+niv, 0);
		}

		private void updateButtonScores() {
			for(int i=1; i<4; i++) {
				LinearLayout b = getButton(i);
				TextView tt = (TextView) ((LinearLayout)b.GetChildAt (2)).GetChildAt (0);
				int niveauScore = getNiveauScore (i);
				tt.Text = "" + niveauScore;
				int score = getNiveauScore (i - 1);
				if (getNiveauScore (i - 1) >= 75) {
					enableButton (b);
				}
			}
		}

		private void enableButton(LinearLayout ll){
			ll.Clickable = true;
			ImageView iv = (ImageView) ll.GetChildAt (0);
			iv.SetImageDrawable (null);
		}

		private LinearLayout getButton(int i){
			LinearLayout b = new LinearLayout (this);
			if (niveau1Button.Tag.ToString ().Equals ("" + i)) {
				b = niveau1Button;
			}
			else if (niveau2Button.Tag.ToString ().Equals ("" + i)) {
				b = niveau2Button;
			}
			else if (niveau3Button.Tag.ToString ().Equals ("" + i)) {
				b = niveau3Button;
			}
			return b;
		}


							
			
	}
}

