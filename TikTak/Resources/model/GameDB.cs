﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Media;

namespace TikTak
{
	public class GameDB {
		public List<List<ImageAudio>> cathegories;
		public int niveau;

		public GameDB(int niveau, Activity a) {
			init (niveau, a);
		}

		private void init(int niveau, Activity a) {

			cathegories = new List<List<ImageAudio>> ();
			this.niveau = niveau;

			if (niveau >= 1) {

				List<ImageAudio> animals = new List<ImageAudio> ();
				animals.Add (new ImageAudio (a, Resource.Drawable.bear, Resource.Raw.bear));
				animals.Add (new ImageAudio (a, Resource.Drawable.bee, Resource.Raw.bee));
				animals.Add (new ImageAudio (a, Resource.Drawable.bird, Resource.Raw.bird));
				animals.Add (new ImageAudio (a, Resource.Drawable.cat, Resource.Raw.cat));
				animals.Add (new ImageAudio (a, Resource.Drawable.chicken, Resource.Raw.chicken));
				animals.Add (new ImageAudio (a, Resource.Drawable.cow, Resource.Raw.cow));
				animals.Add (new ImageAudio (a, Resource.Drawable.cricket, Resource.Raw.cricket));
				animals.Add (new ImageAudio(a, Resource.Drawable.crocodile, Resource.Raw.crocodile));
				animals.Add (new ImageAudio (a, Resource.Drawable.dog, Resource.Raw.dog));
				animals.Add (new ImageAudio (a, Resource.Drawable.doplhin, Resource.Raw.dolphin));
				animals.Add (new ImageAudio (a, Resource.Drawable.eagle, Resource.Raw.eagle));
				animals.Add (new ImageAudio (a, Resource.Drawable.elephant, Resource.Raw.elephant));
				animals.Add (new ImageAudio (a, Resource.Drawable.frog, Resource.Raw.frog));
				animals.Add (new ImageAudio(a, Resource.Drawable.giraffe, Resource.Raw.giraffe));
				animals.Add (new ImageAudio (a, Resource.Drawable.horse, Resource.Raw.horse));
				animals.Add (new ImageAudio (a, Resource.Drawable.lion, Resource.Raw.lion));
				animals.Add (new ImageAudio (a, Resource.Drawable.monkey, Resource.Raw.monkey));
				animals.Add (new ImageAudio(a, Resource.Drawable.moose, Resource.Raw.moose));
				animals.Add (new ImageAudio (a, Resource.Drawable.owl, Resource.Raw.owl));
				animals.Add (new ImageAudio(a, Resource.Drawable.platypus, Resource.Raw.platypus));

				cathegories.Add (animals);

			}
			if (niveau >= 2) {

				List<ImageAudio> alphabet = new List<ImageAudio> ();
				alphabet.Add (new ImageAudio (a, Resource.Drawable.zero, Resource.Raw.zero));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.een, Resource.Raw.een));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.twee, Resource.Raw.twee));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.drie, Resource.Raw.drie));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.vier, Resource.Raw.vier));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.vijf, Resource.Raw.vijf));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.zes, Resource.Raw.zes));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.zeven, Resource.Raw.zeven));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.acht, Resource.Raw.acht));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.negen, Resource.Raw.negen));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.a, Resource.Raw.a));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.b, Resource.Raw.b));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.c, Resource.Raw.c));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.d, Resource.Raw.d));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.e, Resource.Raw.e));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.f, Resource.Raw.f));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.g, Resource.Raw.g));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.h, Resource.Raw.h));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.i, Resource.Raw.i));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.j, Resource.Raw.j));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.k, Resource.Raw.k));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.l, Resource.Raw.l));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.m, Resource.Raw.m));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.n, Resource.Raw.n));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.o, Resource.Raw.o));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.p, Resource.Raw.p));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.q, Resource.Raw.q));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.r, Resource.Raw.r));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.s, Resource.Raw.s));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.t, Resource.Raw.t));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.u, Resource.Raw.u));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.v, Resource.Raw.v));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.w, Resource.Raw.w));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.x, Resource.Raw.x));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.y, Resource.Raw.y));
				alphabet.Add (new ImageAudio (a, Resource.Drawable.z, Resource.Raw.z));

				cathegories.Add (alphabet);

			}

			if (niveau >= 3) {

				List<ImageAudio> cartoons = new List<ImageAudio> ();
				cartoons.Add (new ImageAudio(a, Resource.Drawable.barney, Resource.Raw.barney));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.bart, Resource.Raw.bart));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.butters, 0));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.buzz, Resource.Raw.buzz));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.cartman, Resource.Raw.cartman));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.courage, Resource.Raw.courage));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.dexter, Resource.Raw.dexter));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.donaldduck, Resource.Raw.donaldduck));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.donkey, Resource.Raw.donkey));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.dory, Resource.Raw.dory));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.eeyore, Resource.Raw.eeyore));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.flanders, Resource.Raw.flanders));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.fred, 0));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.gary, Resource.Raw.gary));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.goofy, Resource.Raw.goofy));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.homer, Resource.Raw.homer));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.ike, Resource.Raw.ike));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.kenny, Resource.Raw.kenny));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.kyle, Resource.Raw.kyle));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.marge, Resource.Raw.marge));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.mickeymouse, Resource.Raw.mickey));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.milhouse, 0));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.moe, Resource.Raw.moe));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.mrburns, Resource.Raw.mrburns));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.mrcrabs, Resource.Raw.mrkrabs));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.mrpotatohead, Resource.Raw.mrpotatohead));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.nelson, 0));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.nemo, Resource.Raw.nemo));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.octo, Resource.Raw.octo));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.patrick, Resource.Raw.patrick));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.plankton, Resource.Raw.plankton));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.pooh, Resource.Raw.pooh));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.ralph, Resource.Raw.ralph));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.randy, Resource.Raw.randy));
				cartoons.Add (new ImageAudio(a, Resource.Drawable.sandy, Resource.Raw.sandy));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.spongebob, 0));
				// cartoons.Add (new ImageAudio(a, Resource.Drawable.woody, 0));

				cathegories.Add (cartoons);

			}
		}

		public ImageAudio getRandomImageAudio(int cathegoryindex) {
			Random r = new Random ();
			int rint = r.Next (0, cathegories[cathegoryindex].Count);
			Console.WriteLine ("CategorieIndex: " + cathegoryindex + ", rint: " + rint);
			ImageAudio result = cathegories[cathegoryindex][rint];
			cathegories[cathegoryindex].RemoveAt (rint);
			return result;
		}

		public int getRandomCathegoryIndex() {
			Random r = new Random ();
			int ci = -1;
			int fail = 100;
			while((ci == -1 || cathegories[ci].Count < niveau*2) && fail > 0) {
				ci =  r.Next (0, cathegories.Count);
				fail--;
			}
			return ci;
		}

	}
}

